var admin = require("firebase-admin");

var express = require('express');
var router = express.Router();


router.get('/', function (req, res, next) {
    const uid = req.query.uid;
    admin.auth().getUser(uid).then((userRecord) => {
        res.send(userRecord.customClaims.admin);
    }).catch(() => {
        res.send(false);
    });
});

router.post('/', function (req, res, next) {
    const uid = req.body.uid;
    admin.auth().setCustomUserClaims(uid, {admin: true}).then(() => {
        res.send("Done");
    });

});

module.exports = router;