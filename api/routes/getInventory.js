var admin = require("firebase-admin");
var express = require('express');
var router = express.Router();

var config = require("../Config");

function timestampToString(timestamp) {
    return timestamp.toDate().toLocaleDateString();
}

String.prototype.capitalize = function (lower) {
    return (lower ? this.toLowerCase() : this).replace(/(?:^|\s)\S/g, function (a) {
        return a.toUpperCase();
    });
};

router.get('/body', function (req, res, next) {
    const req_club = req.query.club.toUpperCase();
    const search_query = req.query.search;
    let body = [];
    admin.firestore().collection(`inventory/clubs/${req_club}`).get().then(snapshot => {
        snapshot.forEach(doc => {
            let ref = doc.data();
            let addToData = false;
            for (let key in doc.data()) {
                if (typeof ref[key] === 'object') {
                    const date = timestampToString(ref[key]);
                    ref[key] = date;
                }
                if (ref[key].toLowerCase().includes(search_query.length > 0 && search_query.toLowerCase())) {
                    addToData = true;
                }
            }
            if (addToData || search_query.length === 0)
                body.push(ref);
        })
    }).then(() => {
        res.send(body);
    })
});

router.get('/head', function (req, res, next) {
    const req_club = req.query.club.toUpperCase();
    new Promise((resolve, reject) => {
        let keys = config.defaultInventoryKeys;
        let headers = config.defaultInventoryKeysDict;

        admin.firestore().collection(`inventory/clubs/${req_club}`).get().then(snapshot => {
            snapshot.forEach(doc => {
                for (let key in doc.data()) {
                    if (!keys.includes(key)) {
                        keys.push(key);
                        headers.push({title: key.replace('_', ' ').capitalize(), field: key});
                    }
                }
            })
        }).then(() => {
            console.log(headers);
            res.send(headers)
        })
    });
});

router.post('/', function (req, res, next) {
    const uid = req.body.uid;
    admin.auth().setCustomUserClaims(uid, {admin: true}).then(() => {
        res.send("Done");
    });

});

module.exports = router;
