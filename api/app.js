var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var sassMiddleware = require('node-sass-middleware');
var cors = require('cors');

var claimsRouter = require('./routes/userClaims');
var inventoryRouter = require('./routes/getInventory');
var emailRouter = require('./routes/sendEmail');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(cors());
app.use(sassMiddleware({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: true, // true = .sass and false = .scss
    sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use("/claims", claimsRouter);
app.use("/inv", inventoryRouter);
app.use("/email", emailRouter);

module.exports = app;
