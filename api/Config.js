const defaultInventoryKeys = [
    'make',
    'model',
    'price',
    'date_purchased',
    'expiry_date',
    'docID'
];

const defaultInventoryKeysDict = [
    {title: 'Make', field: 'make', type: 'string'},
    {title: 'Model', field: 'model', type: 'string'},
    {title: 'Price', field: 'price', type: 'currency'},
    {title: 'Date Purchased', field: 'date_purchased', type: 'date'},
    {title: 'Expiry Date', field: 'expiry_date', type: 'date'},
    {title: 'DocID', field: 'docID', type: 'string', hidden: true},
];

module.exports = {
    defaultInventoryKeys: defaultInventoryKeys,
    defaultInventoryKeysDict: defaultInventoryKeysDict,
};
