import {myFirebase, db} from "../Firebase/firebase";
import axios from "axios";
import appConfig from "../Config";

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

export const REGISTER_REQUEST = "REGISTER_REQUEST";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAILURE = "REGISTER_FAILURE";

export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILURE = "LOGOUT_FAILURE";

export const VERIFY_REQUEST = "VERIFY_REQUEST";
export const VERIFY_SUCCESS = "VERIFY_SUCCESS";

const requestLogin = () => {
    return {
        type: LOGIN_REQUEST
    };
};

const receiveLogin = user => {
    return {
        type: LOGIN_SUCCESS,
        user
    };
};

const loginError = () => {
    return {
        type: LOGIN_FAILURE
    };
};

const requestRegister = () => {
    return {
        type: REGISTER_REQUEST
    };
};

const receiveRegister = user => {
    return {
        type: REGISTER_SUCCESS,
        user
    };
};

const RegisterError = () => {
    return {
        type: REGISTER_FAILURE
    };
};

const requestLogout = () => {
    return {
        type: LOGOUT_REQUEST
    };
};

const receiveLogout = () => {
    return {
        type: LOGOUT_SUCCESS
    };
};

const logoutError = () => {
    return {
        type: LOGOUT_FAILURE
    };
};

const verifyRequest = () => {
    return {
        type: VERIFY_REQUEST
    };
};

const verifySuccess = () => {
    return {
        type: VERIFY_SUCCESS
    };
};

export const registerUser = (email, password, fullName, club) => dispatch => {
    dispatch(requestRegister());
    myFirebase.auth().createUserWithEmailAndPassword(email, password)
        .then(user => {
            user.user.updateProfile({
                displayName: fullName
            }).then(() => {
                //if email ends with @gmail.com => admin access
                if (email.endsWith(appConfig.club_admin_email_domain_name)) {
                    axios.post('http://localhost:9000/claims', {
                        uid: user.user.uid
                    }).then(() => {
                        db.collection('clubs').doc(club).set({
                            inventory: `inventory/clubs/${club}`,
                            members: `members/${club}`
                        });
                        db.collection('members').doc(club).set({
                            admin: email,
                            club_members: []
                        });
                        db.collection('users').doc(email).set({
                            admin: club,
                            user_clubs: [club]
                        });
                    });
                }
                //else => standard non-admin access
                else {
                    //creates a document named with user email in the database in users/... => users/example@eg.ie
                    db.collection('users').doc(email).set({user_clubs: []});

                }
            }).then(() => {
                dispatch(receiveRegister())
            })
        })
        .catch(error => {
            //Do something with the error if you want!
            dispatch(RegisterError());
        });
};

export const loginUser = (email, password) => dispatch => {
    dispatch(requestLogin());
    myFirebase.auth().signInWithEmailAndPassword(email, password)
        .then(user => {
            dispatch(receiveLogin(user));
        })
        .catch(error => {
            //Do something with the error if you want!
            dispatch(loginError());
        });
};

export const logoutUser = () => dispatch => {
    dispatch(requestLogout());
    myFirebase.auth().signOut().then(() => {
        dispatch(receiveLogout());
    })
        .catch(error => {
            //Do something with the error if you want!
            dispatch(logoutError());
        });
};

export const updatePassword = (password) => {
    return myFirebase.auth().currentUser.updatePassword(password)
};

export const verifyAuth = () => dispatch => {
    dispatch(verifyRequest());
    myFirebase.auth().onAuthStateChanged(user => {
        if (user !== null) {
            dispatch(receiveLogin(user));
        }
        dispatch(verifySuccess());
    });
};

export const isLoggedIn = () => {
    myFirebase.auth().onAuthStateChanged(function (user) {
        return !!user;
    });
};
