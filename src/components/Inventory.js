import React, {Component, forwardRef} from "react";
import {connect} from "react-redux";
import NavBar from "./templates/NavBar";
import axios from 'axios';
import MaterialTable from "material-table";
import ViewColumnIcon from '@material-ui/icons/ViewColumn';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {db} from "../Firebase/firebase";
import ROUTES from "../constants/routes";
import {handleChange, handleUpload} from "../components/UploadImage";
import UploadImage from "../components/UploadImage";

class Inventory extends Component {

    tableIcons = {
        ViewColumnIcon: forwardRef((props, ref) => <ViewColumnIcon {...props} ref={ref}/>)
    };

    getTableHeaders = new Promise((resolve, reject) => {
        axios.get(`http://localhost:9000/inv/head?club=${this.props.match.params.club}`).then(response => {
            resolve(response.data)
        })
    });

    constructor(props) {
        super(props);
        this.state = {
            headers: [],
            data: [],
            dialogOpen: false,
            addColumn: "",
            uploadDialogOpen: false,
            imageDocID: ""
        };
        this.tableRef = React.createRef();
    }

    componentDidMount() {
        this.getTableHeaders.then((val) => this.setState({headers: val}));
        db.doc(`clubs/${this.props.match.params.club.toUpperCase()}`).get().then(doc => {
            if (!doc.exists) {
                this.props.history.push("/home")
            }
        }).then(() => {
            db.doc(`members/${this.props.match.params.club.toUpperCase()}`).get().then(doc => {
                if (doc.exists) {
                    if (!(doc.data().club_members.includes(this.props.user.email)) && (this.props.user.email !== doc.data().admin)) {
                        this.props.history.push("/home");
                        console.log("NOOOOOO", this.props.user.email, doc.data())
                    }
                } else
                    this.props.history.push("/home");
            })
        });
    }

    handleDialogOpen = () => {
        this.setState({dialogOpen: true})
    };

    handleDialogClosed = () => {
        this.setState({dialogOpen: false})
    };

    handleAddColumnChange = (event) => {
        this.setState({addColumn: event.target.value})
    };

    handleAddColumnSubmit = () => {
        const colName = this.state.addColumn;
        this.setState({
            headers: [...this.state.headers, {
                field: colName.toLowerCase().replace(' ', '_'),
                title: colName
            }]
        })
    };

    handleUploadDialogOpen = () => {
        this.setState({uploadDialogOpen: true})
    };
    handleUploadDialogClose = () => {
        this.setState({uploadDialogOpen: false})
    };

    render() {
        return (
            <div>
                <NavBar title='Inventory' dispatch={this.props.dispatch}/>

                <MaterialTable
                    title={this.props.match.params.club.toUpperCase()}
                    icons={this.tableIcons}
                    tableRef={this.tableRef}
                    columns={this.state.headers}
                    datas={this.state.data}
                    data={query =>
                        new Promise((resolve, reject) => {
                            let url = `http://localhost:9000/inv/body?club=${this.props.match.params.club}&search=${query.search}`;
                            fetch(url)
                                .then(response => response.json())
                                .then(result => {
                                    console.log(result);
                                    resolve({
                                        data: result,
                                    })
                                })
                        })
                    }
                    editable={{
                        onRowAdd: newData =>
                            new Promise((resolve, reject) => {
                                const ref = db.collection(`inventory/clubs/${this.props.match.params.club.toUpperCase()}`)
                                    .doc();
                                newData['docID'] = ref.id;
                                ref.set(newData)
                                    .then(() => {
                                        resolve();
                                    })
                            }),
                        onRowUpdate: (newData, oldData) =>
                            new Promise((resolve, reject) => {
                                db.doc(`inventory/clubs/${this.props.match.params.club.toUpperCase()}/${oldData['docID']}`)
                                    .update(newData).then(() => {
                                    resolve();
                                })
                            }),
                        onRowDelete: oldData =>
                            new Promise((resolve, reject) => {
                                db.doc(`inventory/clubs/${this.props.match.params.club.toUpperCase()}/${oldData['docID']}`)
                                    .delete().then(() => {
                                    resolve();
                                })
                            })
                    }}
                    actions={[
                        {
                            icon: 'refresh',
                            tooltip: 'Refresh Data',
                            isFreeAction: true,
                            onClick: () => this.tableRef.current && this.tableRef.current.onQueryChange(),
                        },
                        {
                            icon: 'image',
                            tooltip: 'Add Image',
                            isFreeAction: false,
                            onClick: (event, row) => {
                                this.handleUploadDialogOpen();
                                this.setState({imageDocID: row.docID})
                            },
                        },
                        {
                            icon: this.tableIcons.ViewColumnIcon,
                            tooltip: 'Add Column',
                            isFreeAction: true,
                            onClick: (event, data) => {
                                this.handleDialogOpen()
                            }
                        }
                    ]}
                    options={{
                        exportButton: true,
                        paging: false,
                        search: true
                    }}
                />

                <Dialog open={this.state.dialogOpen} onClose={this.handleDialogClosed}
                        aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add Column</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please enter the name of the column you wish to add.
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Column Name"
                            type="text"
                            fullWidth
                            onChange={this.handleAddColumnChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDialogClosed} color="primary">
                            Cancel
                        </Button>
                        <Button
                            color="primary"
                            type='submit'
                            onClick={() => {
                                this.handleAddColumnSubmit();
                                this.handleDialogClosed();
                            }}
                        >
                            Add
                        </Button>
                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.uploadDialogOpen} onClose={this.handleUploadDialogClose}
                        aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add Image</DialogTitle>
                    <DialogContent>
                        <UploadImage docID={this.state.imageDocID}/>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDialogClosed} color="primary">Cancel</Button>

                    </DialogActions>
                </Dialog>

                <Dialog open={this.state.dialogOpen} onClose={this.handleDialogClosed}
                        aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add Column</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please enter the name of the column you wish to add.
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Column Name"
                            type="text"
                            fullWidth
                            onChange={this.handleAddColumnChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDialogClosed} color="primary">
                            Cancel
                        </Button>
                        <Button
                            color="primary"
                            type='submit'
                            onClick={() => {
                                this.handleAddColumnSubmit();
                                this.handleDialogClosed();
                            }}
                        >
                            Add
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        isLoggingOut: state.auth.isLoggingOut,
        logoutError: state.auth.logoutError,
        user: state.auth.user
    };
}

export default connect(mapStateToProps)(Inventory);
