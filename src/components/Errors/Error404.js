import React, {Component} from 'react'
import NavBar from "../templates/NavBar";

class Error404 extends Component {
    render() {
        return (
            <div>
                <NavBar title="404 Not Found"/>
                <h1>404! Oh no!</h1>
            </div>
        );
    }
}

export default Error404