import React, {Component} from 'react'
import NavBar from "./templates/NavBar";
import {connect} from "react-redux";

import Button from "@material-ui/core/Button";
import {TextField} from "@material-ui/core";
import {Container, Typography} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import withStyles from "@material-ui/core/styles/withStyles";
import {Redirect} from "react-router-dom";
import {updatePassword} from "../actions";
import {makeStyles} from "@material-ui/styles";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import {db, myFirebase} from "../Firebase/firebase";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Grid from "@material-ui/core/Grid";

const styles = makeStyles(theme => ({
    "@global": {
        body: {
            backgroundColor: "#fff"
        }
    },
    paper: {
        marginTop: 100,
        display: "flex",
        padding: 20,
        flexDirection: "column",
        alignItems: "center"
    },
    avatar: {
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "#f50057"
    },
    form: {
        marginTop: 1
    },
}));

class Account extends Component {

    constructor(props) {
        super(props);
        this.state = {
            claims: "",
            passwordUpdate: "",
            clubs: []
        };
    }

    componentDidMount() {
        this.setUsersClubs();
    }

    //This method initializes the state array variable 'clubs' with with the elements in the 'user_clubs' array
    // of current user in the database => users/example@eg.ie/user_clubs[]
    setUsersClubs() {
        const user = myFirebase.auth().currentUser.email;
        db.doc(`users/${user}`).get()
            .then((snapshot) => {
                if (snapshot.data().user_clubs !== undefined)
                    this.setState({clubs: snapshot.data().user_clubs});
            }).then(() => {
            db.collection('users').doc(user).onSnapshot(doc => {
                this.setState({clubs: doc.data().user_clubs})
            });


        })

            .catch((err) => {
            })
    }

    handlePasswordChange = ({target}) => {
        this.setState({password: target.value});
    };

    handleSnackbarClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({
            passwordUpdate: ""
        });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const password = this.state.password;
        updatePassword(password).then(() => {
            this.setState({
                passwordUpdate: "Success",
            });
            console.log("UPDATED")
        }).catch((error) => {
            console.log("PASSWORD UPDATE FAILED", error);
            this.setState({
                passwordUpdate: "Failed: " + error.message.substr(error.message.indexOf(' ')),
            });
        })
    };

    render() {
        let clubsList = [];

        const {classes, isAuthenticated} = this.props;
        if (isAuthenticated) {
            return <Redirect to="/"/>;
        } else {


            //Create clublist array which will contain the items from user_clubs array in database
            //if user_clubs array in database exists for current user
            if (this.state.clubs !== undefined)
                this.state.clubs.forEach((club) => {
                    clubsList.push(
                        <Button key={club}>{club}</Button>
                    )
                });
            clubsList = this.state.clubs;

        }

        const snackbarMessage = "Password Update " + this.state.passwordUpdate;

        return (
            <div>
                <NavBar title='Account' dispatch={this.props.dispatch}/>

                <Container id="cont2" component="main" maxWidth="xs">
                    <form>
                        <Paper className={classes.paper}>
                            <TextField
                                variant='outlined'
                                margin='normal'
                                fullWidth
                                id='password'
                                name='password'
                                type='password'
                                onChange={this.handlePasswordChange}
                            />
                            <Button onClick={this.handleSubmit} type='submit'>Update Password</Button>
                        </Paper>
                    </form>
                </Container>
                <Container maxWidth={'sm'} style={{marginTop:'2rem'}}>
                    <Paper component={"div"}
                           style={{padding: '2rem 2rem 4rem 2rem', boxShadow: '5px 5px 5px black'}}>
                        <Typography component={"header"}
                                    style={{color: '#3b3b3b', fontSize: '2rem', padding: '1rem'}}>
                            Your Clubs
                        </Typography>
                        <Grid container spacing={3}>
                            <Grid container spacing={1} direction="column" alignItems="center">
                                <Paper style={{
                                    backgroundColor: "#4D5063",
                                    padding: "1rem",
                                    borderRadius: '5px',
                                }}>
                                    {clubsList.join()}
                                </Paper>
                            </Grid>
                        </Grid>
                    </Paper>
                </Container>


                {this.state.passwordUpdate !== "" &&
                <Snackbar
                    open={true}
                    anchorOrigin={{vertical: "bottom", horizontal: "right"}}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="close"
                            color="inherit"
                            className={classes.close}
                            onClick={this.handleSnackbarClose}
                        >
                            <Icon>close</Icon>
                        </IconButton>,
                    ]}
                    message={snackbarMessage}>
                </Snackbar>
                }
            </div>
        );

    }

}

function mapStateToProps(state) {
    return {
        isLoggingOut: state.auth.isLoggingOut,
        logoutError: state.auth.logoutError
    };
}

export default withStyles(styles)(connect(mapStateToProps)(Account));