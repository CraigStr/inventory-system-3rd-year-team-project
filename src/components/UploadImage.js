import React from "react";
import {Component} from "react";
import {writeStorage, writePostData, myFirebase} from "../Firebase/firebase";
import Button from "@material-ui/core/Button";


class UploadImage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            image: null
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleUpload = this.handleUpload.bind(this);
    }

    handleChange(e) {
        if (e.target.files[0]) {
            const image = e.target.files[0];
            this.setState({image: image});
        }
    }

    async handleUpload(event, test) {

        if (this.state.image !== null) {
            const image = this.state.image;
            const storageCall = await writeStorage(image);
            console.log(storageCall);
            console.log(this.props.docID);
            if (storageCall) {
                await writePostData("nobody", this.props.docID + ".png", );
                console.log("UPLOADED !");
            }
        }
    }

    render() {
        return (
            <div className="upload-project">
                <div className="container text-center">
                    <input type="file" id='upload' onChange={this.handleChange}/>
                    <Button onClick={this.handleUpload}>Upload</Button>
                </div>
            </div>
        );
    }
}

export default UploadImage;