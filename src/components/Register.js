import React, {Component} from "react";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import {registerUser} from "../actions";
import {withStyles} from "@material-ui/styles";

import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import NavBar from "./templates/NavBar";
import ROUTES from "../constants/routes";
import appConfig from "../Config";
import {db} from "../Firebase/firebase";

const passwordLength = 6;
const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const adminDomain = appConfig.club_admin_email_domain_name;
const fullNameRegex = /^[a-zA-Z ',.-]{1,100}$/;
const passwordRegex = new RegExp("^.{" + passwordLength + ",}$");
const passwordMessage = 'Password must be at least ' + passwordLength + ' characters long';

const styles = () => ({
    "@global": {
        body: {
            backgroundColor: "#fff"
        }
    },
    paper: {
        marginTop: 100,
        display: "flex",
        padding: 20,
        flexDirection: "column",
        alignItems: "center"
    },
    avatar: {
        marginLeft: "auto",
        marginRight: "auto",
        backgroundColor: "#f50057"
    },
    form: {
        marginTop: 1
    },
    errorText: {
        color: "#f50057",
        marginBottom: 5,
        textAlign: "center"
    }
});

class Register extends Component {
    state = {
        email: "",
        fullName: "",
        passwordOne: "",
        passwordTwo: "",
        showClubName: false,
        clubName: "",
        validClubName: false
    };

    handleEmailChange = ({target}) => {
        this.setState({email: target.value});
        target.value.endsWith(adminDomain) ?
            this.setState({showClubName: true})
            : this.setState({showClubName: false})
    };

    handleFullNameChange = ({target}) => {
        this.setState({fullName: target.value});
    };

    handlePasswordOneChange = ({target}) => {
        this.setState({passwordOne: target.value});
    };

    handlePasswordTwoChange = ({target}) => {
        this.setState({passwordTwo: target.value});
    };

    handleClubNameChange = ({target}) => {
        target.value = target.value.toUpperCase();
        this.setState({clubName: target.value.toUpperCase()});
        this.validateClubName(target.value.toUpperCase())
    };

    handleSubmit = () => {
        const {dispatch} = this.props;
        const {email, fullName, passwordOne, passwordTwo, clubName} = this.state;

        if (!this.validateEmail(email) && !this.validateFullName(fullName) && !this.validatePassword(passwordOne, passwordTwo).value) {
            dispatch(registerUser(email.toLocaleLowerCase(), passwordOne, fullName, email.endsWith(adminDomain) ? clubName : null));
        }
    };

    validateEmail(email) {
        return !emailRegex.test(email);
    }

    validateFullName(name) {
        return !fullNameRegex.test(name);
    }

    validatePassword(passwordOne, passwordTwo) {
        const passOneMatch = passwordRegex.test(passwordOne);
        const passTwoMatch = passwordRegex.test(passwordTwo);
        const passEqual = passwordOne === passwordTwo;
        let message = '';
        if (!passOneMatch)
            message = passwordMessage;
        else if (!passEqual)
            message = 'Passwords not equal';
        return {message: message, value: !(passOneMatch && passTwoMatch && passEqual)};
    }

    validateClubName(clubName) {
        if (clubName.length > 0)
            db.doc(`/clubs/${clubName}`).get().then((doc) => {
                this.setState({validClubName: !doc.exists})
            });
        else
            this.setState({validClubName: false})
    }

    render() {
        const {classes, registerError, isAuthenticated} = this.props;
        if (isAuthenticated) {
            return <Redirect to={ROUTES.HOME}/>;
        } else {
            return (
                <React.Fragment>
                    <NavBar title='Register'/>
                    <Container component="main" maxWidth="xs">
                        <form onSubmit={(event) => {
                            event.preventDefault();
                            this.handleSubmit();
                        }}>
                            <Paper className={classes.paper}>
                                <Avatar className={classes.avatar}>
                                    <LockOutlinedIcon/>
                                </Avatar>
                                <Typography component="h1" variant="h5">
                                    Sign Up
                                </Typography>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="email"
                                    label="Email Address"
                                    type="text"
                                    name="email"
                                    required={true}
                                    error={this.validateEmail(this.state.email)}
                                    onChange={this.handleEmailChange}
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="fullName"
                                    label="Full Name"
                                    type="text"
                                    name="FullName"
                                    required={true}
                                    error={this.validateFullName(this.state.fullName)}
                                    onChange={this.handleFullNameChange}
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    name="passwordOne"
                                    label="Password"
                                    type="password"
                                    id="passwordOne"
                                    required={true}
                                    error={this.validatePassword(this.state.passwordOne, this.state.passwordTwo).value}
                                    onChange={this.handlePasswordOneChange}
                                />
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    name="passwordTwo"
                                    label="Password"
                                    type="password"
                                    id="passwordTwo"
                                    required={true}
                                    helperText={this.validatePassword(this.state.passwordOne, this.state.passwordTwo).message}
                                    error={this.validatePassword(this.state.passwordOne, this.state.passwordTwo).value}
                                    onChange={this.handlePasswordTwoChange}
                                />
                                {registerError && (
                                    <Typography component="p" className={classes.errorText}>
                                        Incorrect email or password.
                                    </Typography>
                                )}
                                {this.state.showClubName && (
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        fullWidth
                                        name="clubName"
                                        label="Club Name"
                                        type="text"
                                        id="clubName"
                                        required={true}
                                        error={!this.state.validClubName}
                                        onChange={this.handleClubNameChange}
                                    />
                                )}
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                >
                                    Sign Up
                                </Button>
                            </Paper>
                        </form>
                    </Container>
                </React.Fragment>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        isRegistering: state.auth.isRegistering,
        registerError: state.auth.registerError,
        isAuthenticated: state.auth.isAuthenticated
    };
}

export default withStyles(styles)(connect(mapStateToProps)(Register));
