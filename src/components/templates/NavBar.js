import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import CssBaseline from '@material-ui/core/CssBaseline';
import {logoutUser} from "../../actions";
import {BrowserRouter as Router} from "react-router-dom";
import ROUTES from "../../constants/routes";
import {myFirebase} from "../../Firebase/firebase";
import {Icon} from "@material-ui/core";
import axios from "axios";
import 'material-icons/iconfont/material-icons.css'
import Layout from "./ResponsiveDrawer";

const styles = theme => ({
    root: {
        flexGrow: 1,

    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        textAlign: "left",
        paddingRight: "20px",
    },
});


class NavBar extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        this.state = {
            user: undefined,
            isAdmin: false
        }
    };

    handleLogout = () => {
        const {dispatch} = this.props;
        dispatch(logoutUser());
        this.setState({
            user: undefined,
            isAdmin: false
        })
    };

    componentDidMount() {
        this._isMounted = true;
        myFirebase.auth().onAuthStateChanged(user => {
                if (user) {
                    let isAdmin = false;
                    axios.get('http://localhost:9000/claims', {
                        params: {
                            uid: user.uid
                        }
                    }).then(res => {

                        isAdmin = res.data;

                    }).then(() => {
                        if (this._isMounted) {
                            this.setState({
                                user: user,
                                isAdmin: isAdmin
                            });
                        }
                    });
                }
            }
        )
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
                <CssBaseline />
                <AppBar style={{backgroundColor: '#a6a1ff'}} position="static" >
                    <Toolbar>
                        <Router>
                            <Layout>
                                <IconButton edge="start" color="inherit" aria-label="menu">
                                    <MenuIcon/>
                                </IconButton>
                            </Layout>
                            <Typography style={{paddingLeft: "20px"}} variant="h6" color="inherit"
                                        className={classes.title}>
                                {this.props.title}
                            </Typography>
                            {!this.state.user ?
                                <React.Fragment>
                                    <Button color="inherit" href={ROUTES.SIGNIN}>Login</Button>
                                    <Button color="inherit" href={ROUTES.SIGNUP}>Register</Button>
                                </React.Fragment> :
                                <React.Fragment>
                                    {
                                        this.state.isAdmin &&
                                        (
                                            <React.Fragment>
                                                <Icon title="Club Admin" fontSize="large">supervisor_account</Icon>
                                                <Button color='inherit' href={ROUTES.ADMIN}>Admin Panel</Button>
                                            </React.Fragment>
                                        )
                                    }
                                    <Button color='inherit' href={ROUTES.ACCOUNT}>
                                        {this.state.user.displayName}
                                    </Button>
                                    <p>{this.state.isAdmin}</p>
                                    <Button color="inherit" onClick={this.handleLogout}>Logout</Button>
                                </React.Fragment>
                            }
                        </Router>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }

}


export default withStyles(styles, {withTheme: true})(NavBar);
