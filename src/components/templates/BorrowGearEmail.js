import React, {Component} from "react";
import axios from 'axios'
import {Paper} from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {db, myFirebase} from "../../Firebase/firebase";
import Typography from "@material-ui/core/Typography";

export class BorrowGearEmail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'Name',
            email: 'craig.stratford.2018@mumail.ie',
            subject: 'Subject',
            message: 'Messgae',
            clubs: [],
            clubToEmail: '',
            itemToEmail: '',
            amountToEmail: '',
        }
    }

    generateClubList = () => {
        let clubList = [];
        db.collection('clubs').get().then(docs => {
            docs.forEach(club => {
                clubList.push(<MenuItem key={club.id} value={club.id}>{club.id}</MenuItem>)
            })
        }).then(() => {
            this.setState({clubs: clubList});
        });
    };

    handleBorrowRequest = (evt) => {
        evt.preventDefault();
        console.log(this.state.clubToEmail, this.state.itemToEmail, this.state.amountToEmail);
        axios.post('http://localhost:9000/email/sendemail', {
            to: this.state.clubToEmail,
            from: myFirebase.auth().currentUser.email,
            items: this.state.itemToEmail,
            amount: this.state.amountToEmail,
        })
    };

    handleClubChange = (evt) => {
        this.setState({clubToEmail: evt.target.value});
    };
    handleItemChange = (evt) => {
        this.setState({itemToEmail: evt.target.value});
    };
    handleAmountChange = (evt) => {
        this.setState({amountToEmail: evt.target.value});
    };

    componentDidMount() {
        this.generateClubList()
    }

    styles = {
        background: '#efefef',
        boxShadow: '3px 3px #BCBCBC',
        borderRadius: '5px',
        padding: '1rem'
    };

    render() {
        return (
            <>
                <Paper
                    component='form'
                    onSubmit={this.handleBorrowRequest}
                    style={this.styles}
                >
                    <Typography variant='h4'>
                    Borrow from another Club
                </Typography>
                    <TextField
                        required
                        multiline
                        fullWidth
                        margin='dense'
                        label="Item"
                        variant="outlined"
                        type='text'
                        onChange={this.handleItemChange}
                    />
                    <TextField
                        required
                        fullWidth
                        margin='dense'
                        label="Amount"
                        type='number'
                        variant="outlined"
                        onChange={this.handleAmountChange}
                    />
                    <FormControl variant='outlined' required fullWidth>
                        <InputLabel id="demo-simple-select-label">Club to borrow from</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={this.state.clubToEmail}
                            onChange={this.handleClubChange}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {this.state.clubs}
                        </Select>
                        <Button type='submit' color='inherit'>
                            Send
                        </Button>
                    </FormControl>
                </Paper>
            </>
        )
    }
}
