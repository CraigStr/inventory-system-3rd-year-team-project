import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/core/SvgIcon/SvgIcon";
import HomeIcon from '@material-ui/icons/Home';
import TocIcon from '@material-ui/icons/Toc';
import LockIcon from '@material-ui/icons/Lock';
import ROUTES from "../../constants/routes";


const useStyles = makeStyles({
    list: {
        width: 250,
        backgroundColor: "#bab7ff",
    },
    fullList: {
        width: 'auto',
        backgroundColor: "#a6a1ff",
    },
    menuButton: {
        marginRight: "auto",
        backgroundColor: "rgb(179,180,255)",

    },
    title: {
        color: "white",
        textDecoration: "none",
    }
});

export default function SwipeableTemporaryDrawer() {
    const classes = useStyles();
    const [state, setState] = React.useState({
        left: false,
    });

    const toggleDrawer = (side, open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({...state, [side]: open});
    };

    const sideList = side => (
        <div
            className={classes.list}
            role="presentation"
            onClick={toggleDrawer(side, false)}
            onKeyDown={toggleDrawer(side, false)}
        >
            <List>
                {['Home'].map((text, index) => (
                    <a className={classes.title} key={text} href={ROUTES.HOME}>
                        <ListItem button key={text}>
                            <ListItemIcon> <HomeIcon/> </ListItemIcon>
                            <ListItemText primary={text}/>
                        </ListItem>
                    </a>
                ))}
            </List>
            <Divider/>
            <List>
                {['Account'].map((text) => (
                    <a className={classes.title} key={text} href={ROUTES.ACCOUNT}>
                        <ListItem button key={text}>
                            <ListItemIcon> <LockIcon/> </ListItemIcon>
                            <ListItemText primary={text}/>
                        </ListItem>
                    </a>
                ))}
            </List>
        </div>
    );
    return (
        <div>
            <IconButton edge="start" className={classes.menuButton}
                        onClick={toggleDrawer('left', true)}>
                <MenuIcon/>
            </IconButton>

            <SwipeableDrawer
                open={state.left}
                onClose={toggleDrawer('left', false)}
                onOpen={toggleDrawer('left', true)}
            >
                {sideList('left')}
            </SwipeableDrawer>

        </div>
    );
}
