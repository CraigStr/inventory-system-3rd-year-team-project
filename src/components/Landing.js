import React, {Component} from "react";
import {connect} from "react-redux";
import NavBar from "./templates/NavBar";
import {withStyles} from "@material-ui/core";
import ROUTES from "../constants/routes";

const styles = () => ({
    "@global": {
        body: {
            backgroundColor: "#fff",
            textAlign: "center",
            fontFamily: "Arial, sans-serif",
        }
    },
});

class Landing extends Component {

    render() {
        console.log(this.props.isAuthenticated);
        if (this.props.isAuthenticated)
            this.props.history.push(ROUTES.HOME);
        return (
            <div>
                <NavBar title='Welcome' dispatch={this.props.dispatch}/>

                <section id="banner">
                    <div className="inner">
                        <header>
                            <h1>Welcome to T3</h1>
                            <h3>Club inventory sorting system <br/> Keep track of your items easily and efficiently</h3>
                        </header>
                        <div><br/><br/></div>
                        <div>
                            <a href={ROUTES.SIGNIN} className="button">Login</a>
                            <br/><br/>
                            <h3>or <a style={{textDecorationLine: "underline"}} href={ROUTES.SIGNUP}>Sign-up</a></h3>

                        </div>


                    </div>
                </section>

            </div>


        );
    }
}

function mapStateToProps(state) {
    return {
        isLoggingOut: state.auth.isLoggingOut,
        logoutError: state.auth.logoutError,
        isAuthenticated: state.auth.isAuthenticated,
    };
}

//export default connect(mapStateToProps)(Landing);
export default withStyles(styles)(connect(mapStateToProps)(Landing));
