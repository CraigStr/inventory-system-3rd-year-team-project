import React, {Component} from 'react'

import NavBar from "./templates/NavBar";
import {BorrowGearEmail} from './templates/BorrowGearEmail';

import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import {firestore} from "firebase/app";

import {db, myFirebase} from "../Firebase/firebase";

import IconButton from "@material-ui/core/IconButton";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {Container, Dialog, Paper, TextField, Typography} from "@material-ui/core";
import Icon from "@material-ui/core/Icon";
import AddIcon from '@material-ui/icons/Add';
import {makeStyles} from "@material-ui/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";

const styles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3, 2),
        display: 'flex',
        alignItems: 'center',
        width: 400,
        borderRadius: '20px'
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        height: 28,
        margin: 4,
    },
}));

class Admin extends Component {

    styles = {
        background: '#efefef',
        boxShadow: '3px 3px #BCBCBC',
        borderRadius: '5px',
        padding: '1rem'
    };

    constructor(props) {
        super(props);
        this.state = {
            members: [],
            addMember: '',
            removeMember: '',
            dialogOpen: false,
            club: ""
        };
    }

    componentDidMount() {
        this.setClubMembers();
    }

    getClubFromUser = () => {
        return new Promise(resolve => {
            const user = myFirebase.auth().currentUser.email;
            db.collection('users').doc(user).get().then(snapshot => {
                resolve(snapshot.data().admin);
            });
        })
    };

    setClubMembers() {
        this.getClubFromUser().then(club => {
            this.setState({club: club});
            db.doc(`members/${club}`).get()
                .then((snapshot) => {
                    if (snapshot.data().club_members !== undefined)
                        this.setState({members: snapshot.data().club_members});
                }).then(() => {
                db.collection('members').doc(this.state.club).onSnapshot(doc => {
                    this.setState({members: doc.data().club_members})
                });

            })
                .catch((err) => {
                })
        });
    }

    handleAddMember = (event) => {
        db.collection('members').doc(this.state.club)
            .update('club_members', firestore.FieldValue.arrayUnion(this.state.addMember)).then(() => {
        });
    };

    handleRemoveMember = (event) => {
        db.collection('members').doc(this.state.club)
            .update('club_members', firestore.FieldValue.arrayRemove(this.state.removeMember)).then(() => {
        });
        this.handleDialogClose();
    };

    handleMemberChange = (event) => {
        this.setState({addMember: event.target.value});
    };

    handleDialogOpen = (member) => {
        this.setState({removeMember: member});
        this.setState({dialogOpen: true});
    };

    handleDialogClose = () => {
        this.setState({dialogOpen: false})
    };

    render() {
        const {isAuthenticated} = this.props;
        if (isAuthenticated) {
            return <Redirect to="/"/>;

        } else {
            const classes = styles;
            let membersList = [];
            if (this.state.members !== undefined)
                this.state.members.forEach((member) => {
                    membersList.push(
                        <ListItem button key={member}>
                            <ListItemText primary={member}/>
                            <IconButton onClick={() => this.handleDialogOpen(member)} aria-label="delete">
                                <Icon value={member} key={member}>delete</Icon>
                            </IconButton>
                        </ListItem>
                    )
                });

            return (
                <div>
                    <NavBar title='Admin Panel' dispatch={this.props.dispatch}/>
                    <br/>
                    <Container maxWidth='xs'>
                        <Paper
                            component="form"
                            className={classes.root}
                            style={this.styles}
                            onSubmit={(e) => {
                                e.preventDefault();
                                this.handleAddMember();
                            }}
                        >
                            <Typography variant='h4'>
                                Members
                            </Typography>
                            <List id='memberList'>
                                {membersList}
                            </List>
                            <TextField
                                fullWidth={true}
                                className={classes.input}
                                type='email'
                                id='addMember'
                                label="Email Address"
                                placeholder="joe.bloggs@example.com"
                                onChange={this.handleMemberChange}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position='end'>
                                            <IconButton
                                                type='submit'
                                            >
                                                <AddIcon/>
                                            </IconButton>
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Paper>
                        <br/>
                        <Divider/>
                        <br/>
                        <BorrowGearEmail/>
                    </Container>


                    <Dialog
                        open={this.state.dialogOpen}
                        onClose={this.handleDialogClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Remove this User?"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Are you sure you want to remove {this.state.removeMember} from this Club?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleDialogClose} color="primary">
                                Disagree
                            </Button>
                            <Button onClick={this.handleRemoveMember} color="primary" autoFocus>
                                Agree
                            </Button>
                        </DialogActions>
                    </Dialog>

                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    return {
        isLoggingOut: state.auth.isLoggingOut,
        logoutError: state.auth.logoutError
    };
}

export default connect(mapStateToProps)(Admin);
