import React, {Component} from "react";
import {connect} from "react-redux";
import {db} from "../Firebase/firebase";
import NavBar from "./templates/NavBar";
import {Container, ListItemText, Typography} from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";


class Metric extends Component {

    constructor(props) {
        super(props);
        this.state = {
            numberProducts: 0,
            numberProductsNearExpiry: 0,
            expiryProducts: [],
            price: 0,
        };
    }

    componentDidMount() {
        db.doc(`clubs/${this.props.match.params.club.toUpperCase()}`).get().then(doc => {
            if (!doc.exists) {
                this.props.history.push("/")
            }
        }).then(() => {
            db.doc(`members/${this.props.match.params.club.toUpperCase()}`).get().then(doc => {
                if (doc.exists) {
                    if (!(doc.data().club_members.includes(this.props.user.email)) && (this.props.user.email !== doc.data().admin)) {
                        this.props.history.push("/");
                    }
                } else
                    this.props.history.push("/");
            })
        });
        this.getNumberProducts();
        this.getNumberProductsNearExpiry();
    }

    getNumberProducts = () => {
        let price = 0;
        db.collection(`inventory/clubs/${this.props.match.params.club.toUpperCase()}`).get().then(snap => {
            const size = snap.size; // will return collection size
            snap.forEach(doc => {
                price += parseInt(doc.data().price);
            });
            this.setState({numProducts: size, price: price});
        });
    };
    getNumberProductsNearExpiry = () => {
        let count = 0;
        let products = [];
        db.collection(`inventory/clubs/${this.props.match.params.club.toUpperCase()}`).where("expiry_date", "<=", new Date(Date.now() + 2592000000))
            .get()
            .then(querySnapshot => {
                querySnapshot.forEach(function (doc) {
                    count++;
                    products.push(<List style={{padding: '0'}}>
                        <ListItem
                            button><ListItemText>{doc.data().make} {doc.data().model} : {doc.data().expiry_date.toDate().toLocaleDateString()}</ListItemText></ListItem>
                    </List>)
                });
            }).then(() => {
            this.setState({
                numberProductsNearExpiry: count,
                expiryProducts: products,
            });
        });
    };

    render() {
        return (
            <>
                <NavBar title={`${this.props.match.params.club.toUpperCase()} Metrics`} dispatch={this.props.dispatch}/>
                <Container style={{marginTop: '2rem'}} maxWidth={'sm'}>
                    <span>
                        <Typography component={'h3'}
                                    style={{padding: '1rem 0 1rem 0'}}>Num Products: {this.state.numProducts}
                        </Typography>
                    </span>
                    <br/>
                    <span>
                        <Typography component={'h3'}
                                    style={{padding: '1rem 0 1rem 0'}}>
                            Num Products Expiry: {this.state.numberProductsNearExpiry}
                        </Typography>
                    </span>
                    <br/>
                    <span>
                        <Typography component={'h3'}
                                    style={{padding: '1rem 0 1rem 0'}}>Products Expiry:
                        </Typography>
                        {this.state.expiryProducts}
                    </span>
                    <br/>
                    <span>
                        <Typography component={'h3'}
                                    style={{padding: '1rem 0 1rem 0'}}>Total Price: {this.state.price}
                        </Typography>
                    </span>
                </Container>
            </>
        )
    }


}


function mapStateToProps(state) {
    return {
        isLoggingOut: state.auth.isLoggingOut,
        logoutError: state.auth.logoutError,
        user: state.auth.user
    };
}

export default connect(mapStateToProps)(Metric);
