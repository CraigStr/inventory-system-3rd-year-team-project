import React, {Component} from "react";
import {connect} from "react-redux";
import NavBar from "./templates/NavBar";
import {db, myFirebase} from '../Firebase/firebase';
import {Container, Typography} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import ButtonGroup from "@material-ui/core/ButtonGroup";

class Home extends Component {

    style = {
        color: 'black'
    };

    constructor(props) {
        super(props);
        this.state = {isAdmin: ""};
        this.state.clubs = [];
    }

    componentDidMount() {
        setTimeout(() => {
            myFirebase.auth().onAuthStateChanged(user => {
                if (user) {
                    db.doc(`users/${user.email}`).get().then(doc => {
                        if (doc.data().hasOwnProperty('user_clubs'))
                            this.setState({clubs: new Set([...this.state.clubs, ...doc.data().user_clubs])});
                        if (doc.data().admin)
                            this.setState({clubs: new Set([...this.state.clubs, doc.data().admin])});
                        console.log(this.state.clubs)
                    })
                }
            })
        }, 2000)
    }

    render() {
        let club_list = [];
        this.state.clubs.forEach(item => {
            club_list.push(
                <Grid item style={{padding: '3px 0 3px 0'}}>
                    <ButtonGroup
                        size="medium"
                    >
                        <Button key={item} href={`inventory/${item.toLowerCase()}`}>{item} Inventory</Button>
                        <Button key={item} href={`metric/${item.toLowerCase()}`}>{item} Metrics</Button>
                    </ButtonGroup>
                </Grid>)
        });
        return (
            <>
                <section id="banner">
                    <NavBar title='Home' dispatch={this.props.dispatch}/>

                    <div className="inner">
                        <header>
                            <h1>Welcome to T3</h1>
                        </header>

                        <Container maxWidth={'sm'}>
                            <Paper component={"div"}
                                   style={{padding: '2rem 2rem 4rem 2rem', boxShadow: '10px 10px 10px black'}}>
                                <Typography component={"header"}
                                            style={{color: '#3b3b3b', fontSize: '2rem', padding: '1rem'}}>
                                    Your Clubs
                                </Typography>
                                <Grid container spacing={3}>
                                    <Grid container spacing={1} direction="column" alignItems="center">
                                        <Paper style={{
                                            backgroundColor: "#4D5063",
                                            padding: "1rem",
                                            borderRadius: '5px',
                                        }}>
                                            {club_list}
                                        </Paper>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </Container>
                    </div>
                </section>
            </>


        );
    }
}

function mapStateToProps(state) {
    return {
        isLoggingOut: state.auth.isLoggingOut,
        logoutError: state.auth.logoutError,
        user: state.auth.user
    };
}

export default connect(mapStateToProps)(Home);
