import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";
import "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyBz4GoD1m3tDl7Th85FpH69N70DcpCL0Es",
    authDomain: "cs353-t3.firebaseapp.com",
    databaseURL: "https://cs353-t3.firebaseio.com",
    projectId: "cs353-t3",
    storageBucket: "cs353-t3.appspot.com",
    messagingSenderId: "860782477175",
    appId: "1:860782477175:web:94a16ad559413f816aab72",
    measurementId: "G-GW6RX7S430"
};

export const myFirebase = firebase.initializeApp(firebaseConfig);
const baseDb = myFirebase.firestore();
export const db = baseDb;



let userAuth = {
    located: null,
    Error: null,
    post: null,
    postKey: 0,
    found: false
};

function errorHandler(e) {
    if (e) {
        return (userAuth.Error = true);
    } else {
        return (userAuth.Error = false);
    }
}

async function readMaxKey() {
    const fetch = firebase.database().ref("mPosts/");
    const getKey = await fetch.once("value");
    const val = getKey.val();
    return (userAuth.postKey = val);
}

export async function writePostData(title, body, userName, imageName) {
    await readMaxKey();

    let post = {
        imageName: imageName
    };
    firebase
        .database()
        .ref("posts/" + ++userAuth.postKey)
        .set(post, errorHandler);
    if (!userAuth.Error) {
        firebase
            .database()
            .ref("mPosts/")
            .set(userAuth.postKey, errorHandler);
    }
    return userAuth.Error;
}

export async function writeStorage(image) {
    const storageUpload = firebase.storage().ref();
    const checkFinish = storageUpload.child(image.name).put(image);

    if (checkFinish.snapshot.ref !== null) {
        return true;
    } else {
        return false;
    }

}

export async function readStorage(imageName) {
    let image = firebase.storage.ref().child(imageName);
    let reader = new FileReader();
    reader.onLoad = function (event) {
        image = event.result;
    }
    reader.readDataAsUrl(image);
    return image;
}

export function readPostData(postKey) {
    firebase
        .database()
        .ref("posts/" + postKey)
        .once("value")
        .then(function (snap) {
            if (snap.exists()) {
                return (userAuth.post = snap.val());
            } else {
                return (userAuth.post = false);
            }
        })
        .catch(errorHandler);
    return userAuth.post;
}
