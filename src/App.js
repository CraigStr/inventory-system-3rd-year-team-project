import React from "react";

import {Route, Switch} from "react-router-dom";
import {connect} from "react-redux";
import ROUTES from "./constants/routes";

import ProtectedRoute from "./components/ProtectedRoute";
import Home from "./components/Home";
import Login from "./components/Login";
import Register from "./components/Register";
import Landing from "./components/Landing";
import Account from "./components/Account";
import Error404 from "./components/Errors/Error404";
import Inventory from "./components/Inventory";
import Admin from "./components/Admin";
import Metric from "./components/Metric";


function App(props) {

    const {isAuthenticated, isVerifying} = props;
    return (
        <Switch>
            <Route exact path={ROUTES.LANDING} component={Landing}/>
            <Route path={ROUTES.SIGNIN} component={Login}/>
            <Route path={ROUTES.SIGNUP} component={Register}/>
            <ProtectedRoute
                path={ROUTES.HOME}
                component={Home}
                isAuthenticated={isAuthenticated}
                isVerifying={isVerifying}
            />
            <ProtectedRoute
                path={ROUTES.INVENTORY}
                component={Inventory}
                isAuthenticated={isAuthenticated}
                isVerifying={isVerifying}
            />
            <ProtectedRoute
                path={ROUTES.ACCOUNT}
                component={Account}
                isAuthenticated={isAuthenticated}
                isVerifying={isVerifying}
            />
            <ProtectedRoute
                path={ROUTES.ADMIN}
                component={Admin}
                isAuthenticated={isAuthenticated}
                isVerifying={isVerifying}
            />
            <ProtectedRoute
                path={ROUTES.METRIC}
                component={Metric}
                isAuthenticated={isAuthenticated}
                isVerifying={isVerifying}
            />
            <Route component={Error404}/>
        </Switch>
    );
}

function mapStateToProps(state) {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        isVerifying: state.auth.isVerifying
    };
}

export default connect(mapStateToProps)(App);
