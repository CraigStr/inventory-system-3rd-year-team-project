const ROUTES = {
    LANDING: '/',
    HOME: '/home',
    SIGNIN: '/login',
    SIGNUP: '/register',
    ACCOUNT: '/account',
    ADMIN: '/admin',
    INVENTORY: '/inventory/:club',
    METRIC: '/metric/:club',
};

export default ROUTES
