# Running the web 
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `node bin/www`
Runs the backend server (http://localhost:9000)

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!  
___
# Using the app
Any user registered with a `@gmail.com` email domain will be able to create a club.  
(This is customisable in the `src/Config.js` file)  

Inventory and Metrics links are present on the home page ONLY when you are a added member of a club. Admins will always be a member of whatever club they created.
___
The Inventory Table has the following Functionality.
* Manipulate Table
  * Add Product to the table
  * Remove Product from the table
  * Edit a Product in the table
  * Add column to the table
* Filter
  * Search
  * Rearrange Headers
  * Sort by Header (asc/desc)
* Refresh Data
* Download as .csv
* Add image to database (WIP)
___
The Metrics page shows valuable information about the clb's inventory
* Total Assets (total price of all products)
* Number of products
* Number of products that are within 1 month of expiry
  * What products are within 1 month of expiry
___
The Accounts Page alows the user to update their password and to see the clubs that they have access to currently.
___
The Admin Panel (Only accessible to club admins) allows the admin to add users to the club and to send an email to the admin of another club on the system requesting to borrow a certain amount of an item.